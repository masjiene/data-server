module.exports = {
	env: {
		browser: false,
		es6: true,
	},
	extends: 'eslint:recommended',
	parserOptions: {
		ecmaVersion: 2018,
		sourceType: 'module',
	},
	rules: {
		'constructor-super': 'error',
		'linebreak-style': ['error', 'unix'],
		semi: ['error', 'always'],
		'no-console': 'off',
	},
}
