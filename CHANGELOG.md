# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

# [1.3.0](https://bitbucket.org/masjiene/data-server/compare/v1.2.0...v1.3.0) (2019-03-10)


### Bug Fixes

* **self:** sub broken ([148efb6](https://bitbucket.org/masjiene/data-server/commits/148efb6))
* **self:** web socket error ([c05a17d](https://bitbucket.org/masjiene/data-server/commits/c05a17d))
* **self:** websocket error trying to fix ([3eb0652](https://bitbucket.org/masjiene/data-server/commits/3eb0652))
* **self:** websocket fix ([ceb5e03](https://bitbucket.org/masjiene/data-server/commits/ceb5e03))
* **self:** ws fixes ([9125348](https://bitbucket.org/masjiene/data-server/commits/9125348))
* **self:** wss fix ([46c9676](https://bitbucket.org/masjiene/data-server/commits/46c9676))


### Features

* **self:** added websockets to get latest db changes ([fed25dc](https://bitbucket.org/masjiene/data-server/commits/fed25dc))
* **self:** old graphql sub cleanup ([e6946a3](https://bitbucket.org/masjiene/data-server/commits/e6946a3))
* **self:** removed websockets and added graphql returns ([6b4c662](https://bitbucket.org/masjiene/data-server/commits/6b4c662))



<a name="1.2.0"></a>
# [1.2.0](https://bitbucket.org/masjiene/data-server/compare/v1.1.0...v1.2.0) (2019-02-28)


### Bug Fixes

* **all:** cookie max age set to days ([7e1cdea](https://bitbucket.org/masjiene/data-server/commits/7e1cdea))
* **all:** login refresh bug ([c91103c](https://bitbucket.org/masjiene/data-server/commits/c91103c))
* **all:** login refresh bug ([7fc9fda](https://bitbucket.org/masjiene/data-server/commits/7fc9fda))
* **all:** maxage removed and session cookie settings used ([09f49e4](https://bitbucket.org/masjiene/data-server/commits/09f49e4))
* **all:** oops ([52b8762](https://bitbucket.org/masjiene/data-server/commits/52b8762))
* **self:** console.log removed ([4e64596](https://bitbucket.org/masjiene/data-server/commits/4e64596))
* **self:** security risk consoling out details ([b1291f2](https://bitbucket.org/masjiene/data-server/commits/b1291f2))
* **self:** signup id issue fixed ([f2949a8](https://bitbucket.org/masjiene/data-server/commits/f2949a8))
* **self:** signup new userid not working ([13307a9](https://bitbucket.org/masjiene/data-server/commits/13307a9))
* **self:** username trime and lowercase ([8f71d1e](https://bitbucket.org/masjiene/data-server/commits/8f71d1e))
* **slef:** signup added email trim and case matching ([8d5d72a](https://bitbucket.org/masjiene/data-server/commits/8d5d72a))


### Features

* **all:** change cookie setting ([2d9a0db](https://bitbucket.org/masjiene/data-server/commits/2d9a0db))
* **all:** changed cokkies again ([da56db9](https://bitbucket.org/masjiene/data-server/commits/da56db9))
* **all:** major refactor of auth ([db59d54](https://bitbucket.org/masjiene/data-server/commits/db59d54))
* **all:** more changes to cookies ([9fe6180](https://bitbucket.org/masjiene/data-server/commits/9fe6180))
* **self:** added create room schema and reslover ([e12df69](https://bitbucket.org/masjiene/data-server/commits/e12df69))
* **self:** added https ([39da5d7](https://bitbucket.org/masjiene/data-server/commits/39da5d7))
* **self:** added signup capabilities ([c06d553](https://bitbucket.org/masjiene/data-server/commits/c06d553))
* **self:** added the ability to create a house ([12a2fe5](https://bitbucket.org/masjiene/data-server/commits/12a2fe5))
* **self:** added websocket server to server changes from mongodb ([4df330b](https://bitbucket.org/masjiene/data-server/commits/4df330b))
* **self:** adding graphql mutations ([438abdf](https://bitbucket.org/masjiene/data-server/commits/438abdf))
* **self:** graphql mutations are working ([4a1cb6a](https://bitbucket.org/masjiene/data-server/commits/4a1cb6a))
* **self:** GraphQL Subscriptions ([0618f62](https://bitbucket.org/masjiene/data-server/commits/0618f62))
* **self:** graphql subscriptions working!!!! ([b01af72](https://bitbucket.org/masjiene/data-server/commits/b01af72))
* **self:** more changes to hosting ([3f1f244](https://bitbucket.org/masjiene/data-server/commits/3f1f244))
* **self:** more cookie settings ([1cc11ef](https://bitbucket.org/masjiene/data-server/commits/1cc11ef))
* **self:** npm audit fix ([bf71717](https://bitbucket.org/masjiene/data-server/commits/bf71717))
* **self:** rooms split ([49bf16d](https://bitbucket.org/masjiene/data-server/commits/49bf16d))
* **self:** webpack changed again ([d798d77](https://bitbucket.org/masjiene/data-server/commits/d798d77))
* **self:** webpack changes ([e517fa7](https://bitbucket.org/masjiene/data-server/commits/e517fa7))
* **self:** webpack dist cleanup changed ([d1810e7](https://bitbucket.org/masjiene/data-server/commits/d1810e7))



<a name="1.1.0"></a>
# [1.1.0](https://bitbucket.org/masjiene/data-server/compare/v1.0.1...v1.1.0) (2019-02-07)


### Bug Fixes

* **data server:** Added users and ready for adding auth ([77411ac](https://bitbucket.org/masjiene/data-server/commits/77411ac))
* **data server:** helping set auth guards ([22cf287](https://bitbucket.org/masjiene/data-server/commits/22cf287))
* **data server:** massive changes ([4effd22](https://bitbucket.org/masjiene/data-server/commits/4effd22))
* **data server:** Modified graphql schema and data return ([0395e13](https://bitbucket.org/masjiene/data-server/commits/0395e13))
* **data server:** query and mutation for depth on object ([bc7af31](https://bitbucket.org/masjiene/data-server/commits/bc7af31))
* **data-server:** added npm build ([d291902](https://bitbucket.org/masjiene/data-server/commits/d291902))
* **data-server:** damn package-lock.json again! somebody save me! ([eb73288](https://bitbucket.org/masjiene/data-server/commits/eb73288))
* **whole app:** removed the userid as required param, get userid from token ([05a2fbf](https://bitbucket.org/masjiene/data-server/commits/05a2fbf))


### Features

* **data app:** added set cookie headers ([1a641a9](https://bitbucket.org/masjiene/data-server/commits/1a641a9))
* **data server:** added ability to run tests ([0126900](https://bitbucket.org/masjiene/data-server/commits/0126900))
* **data server:** added error handling ([c45902c](https://bitbucket.org/masjiene/data-server/commits/c45902c))
* **data server:** added firt failing tests ([d51f357](https://bitbucket.org/masjiene/data-server/commits/d51f357))
* **data server:** Added more graphql queries and mutations ([1395750](https://bitbucket.org/masjiene/data-server/commits/1395750))
* **data server:** added much more complex schemas ([dac7812](https://bitbucket.org/masjiene/data-server/commits/dac7812))
* **data server:** added working tests ([9f79889](https://bitbucket.org/masjiene/data-server/commits/9f79889))
* **data server:** changed more server stuff porty thingy ([90e903c](https://bitbucket.org/masjiene/data-server/commits/90e903c))
* **data server:** device types added ([6c24525](https://bitbucket.org/masjiene/data-server/commits/6c24525))
* **data server:** made test more descriptive ([1078b68](https://bitbucket.org/masjiene/data-server/commits/1078b68))
* **data server:** not sure what I am doing is right ([aa5cb8e](https://bitbucket.org/masjiene/data-server/commits/aa5cb8e))
* **Data server:** added mocking for front-end, some minor changes required on be ([bcc5c25](https://bitbucket.org/masjiene/data-server/commits/bcc5c25))
* **data server & web app:** Added auth and protected routes ([0f1f441](https://bitbucket.org/masjiene/data-server/commits/0f1f441))
* **data-server:** https, ssl attempt ([284ebb7](https://bitbucket.org/masjiene/data-server/commits/284ebb7))
* **self:** Changed some config ([635747a](https://bitbucket.org/masjiene/data-server/commits/635747a))
* **self:** major webpack refactor ([1d690f1](https://bitbucket.org/masjiene/data-server/commits/1d690f1))



<a name="1.0.1"></a>
## 1.0.1 (2018-12-16)


### Bug Fixes

* **Project:** Build is working ([afb7da5](https://bitbucket.org/masjiene/data-server/commits/afb7da5))
* **Server:** Added new npm scripts ([c4070fe](https://bitbucket.org/masjiene/data-server/commits/c4070fe))
* **Server:** Fiddle ([7107245](https://bitbucket.org/masjiene/data-server/commits/7107245))
