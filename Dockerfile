FROM node

MAINTAINER Francois van der Merwe (swyser.co.za)

WORKDIR /home/node/app

VOLUME [ "./dist:/home/node/app" ]

EXPOSE 8082

# docker build -t swyserapp/dataserver .
# docker run --rm -p 8082:8082/tcp --name swyserappdataserver -dit swyserapp/dataserver
