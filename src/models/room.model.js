const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const roomSchema = new Schema({
	name: {
		type: String,
		required: true,
	},
	description: {
		type: String,
		required: false,
	},
	devices: [
		{
			type: Schema.Types.ObjectId,
			ref: 'Device',
		},
	],
});

module.exports = mongoose.model('Room', roomSchema);
