const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const deviceSchema = new Schema({
	name: {
		type: String,
		required: false,
	},
	address: {
		type: String,
		required: false,
	},
	state: {
		type: Number,
		required: false,
		default: 0,
	},
	command: {
		type: String,
		required: false,
	},
	type: {
		type: String,
		required: false,
	},
	port: {
		type: String,
		required: false,
	},
});

module.exports = mongoose.model('Device', deviceSchema);
