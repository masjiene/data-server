const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const houseSchema = new Schema({
	userId: {
		type: String,
		required: true,
	},
	details: {
		name: {
			type: String,
			required: true,
		},
		description: {
			type: String,
			required: false,
		},
		location: {
			lat: {
				type: String,
				required: false,
			},
			long: {
				type: String,
				required: false,
			},
		},
	},
	rooms: [
		{
			type: Schema.Types.ObjectId,
			ref: 'Room',
		},
	],
});

module.exports = mongoose.model('House', houseSchema);
