require('@babel/polyfill');
const express = require('express');
const bodyParser = require('body-parser');
const graphqlHttp = require('express-graphql');
const mongoose = require('mongoose');
const isAuth = require('./utils/is-auth');
const schema = require('./graphql/schemas');
const http = require('http');

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(isAuth);

app.use('/api', (req, res) => {
	return graphqlHttp({
		schema,
		graphiql: process.env.NODE_ENV === 'development',
		subscriptionsEndpoint: '/subscriptions',
	})(req, res);
});

const server = http.createServer(app);

server.listen(8082);

var dbURI = `mongodb+srv://${process.env.MONGO_USER}:${
	process.env.MONGO_PASSWORD
}@swyserdb-jfjw9.mongodb.net/${process.env.MONGO_DB}?retryWrites=true`;

mongoose.connect(dbURI, { useNewUrlParser: true });

mongoose.connection.on('connected', function() {
	console.log('Mongoose default connection open.');
});

mongoose.connection.on('error', function(err) {
	console.log('Mongoose default connection error: ' + err);
});

mongoose.connection.on('disconnected', function() {
	console.log('Mongoose default connection disconnected');
	mongoose.connect(dbURI, { server: { auto_reconnect: true } });
});

module.exports = app;
