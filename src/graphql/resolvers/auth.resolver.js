const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const User = require('../../models/user.model');

module.exports = {
	users: async (args, context) => {
		if (!context.req.isAuth) {
			throw new Error('Unauthorized!');
		}
		return await User.find()
			.then(user => {
				return user.map(user => {
					return { ...user._doc, _id: user.id };
				});
			})
			.catch(err => {
				throw err;
			});
	},
	logout: async (args, context) => {
		if (process.env.NODE_ENV === 'production') {
			context.res.cookie('Authorization', `asdf`, {
				domain: '.swyser.app',
				path: '/',
				secure: true,
				httpOnly: true,
				maxAge: 0,
				overwrite: true,
				encode: String,
			});
			context.res.cookie('user', `asdf`, {
				domain: '.swyser.app',
				path: '/',
				maxAge: 0,
				overwrite: true,
				encode: String,
			});
		} else {
			context.res.cookie('Authorization', `asdf`, {
				maxAge: 0,
				overwrite: true,
				encode: String,
			});
			context.res.cookie('user', `asdf`, {
				maxAge: 0,
				overwrite: true,
				encode: String,
			});
		}

		return await null;
	},
	createUser: async (args, context) => {
		try {
			const existingUser = await User.findOne({ email: args.email });
			if (existingUser) {
				throw new Error('User exists already.');
			}

			const hashedPassword = await bcrypt.hash(args.password, 12);
			const user = new User({
				email: args.email.toLowerCase().trim(),
				password: hashedPassword,
			});

			await user.save();

			return { userId: user.id };
		} catch (err) {
			throw err;
		}
	},
	login: async (args, context) => {
		const user = await User.findOne({
			email: args.email.toLowerCase().trim(),
		});
		if (!user) {
			throw new Error('Login failed');
		}
		const isEqual = await bcrypt.compare(args.password, user.password);
		if (!isEqual || !user) {
			const err = new Error('Login failed');
			throw err;
		}
		const token = jwt.sign(
			{ userId: user._id, email: user.email },
			process.env.SECRET
		);

		if (process.env.NODE_ENV === 'production') {
			context.res.cookie('Authorization', `Bearer ${token};`, {
				domain: '.swyser.app',
				path: '/',
				secure: true,
				httpOnly: true,
				expires: 0,
				encode: String,
			});
			context.res.cookie('user', `${user._id}`, {
				domain: '.swyser.app',
				path: '/',
				expires: 0,
				encode: String,
			});
		} else {
			context.res.cookie('Authorization', `Bearer ${token};`, {
				expires: 0,
				encode: String,
			});
			context.res.cookie('user', `${user._id}`, {
				expires: 0,
				encode: String,
			});
		}

		return { userId: user._id, token: token, tokenExpiration: 1 };
	},
	deleteUser: async (args, context) => {
		if (!context.req.isAuth) {
			throw new Error('Unauthorized!');
		}

		const userToBeDeleted = await User.findOne(
			{ email: args.email },
			(err, doc) => {
				if (!err) {
					doc.remove();
				} else {
					throw new Error('User does not exist.');
				}
			}
		);

		return await userToBeDeleted;
	},
};
