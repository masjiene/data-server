const mongoose = require('mongoose');
const House = require('../../models/house.model');
const Room = require('../../models/room.model');
const Device = require('../../models/device.model');

module.exports = {
	house: async (args, req) => {
		if (!req.isAuth) {
			throw new Error('Unauthorized!');
		}

		return await House.findOne({ userId: req.userId })
			.then(result => {
				return result;
			})
			.catch(err => {
				throw err;
			});
	},
	createHouse: async (args, req) => {
		if (!req.isAuth) {
			throw new Error('Unauthorized!');
		}

		const house = new House({
			userId: req.userId,
			details: args.details,
		});

		return await house.save();
	},
	rooms: async (parent, args, req) => {
		if (!req.isAuth) {
			throw new Error('Unauthorized!');
		}

		return await Room.find({ _id: parent.rooms })
			.then(result => {
				return result;
			})
			.catch(err => {
				throw err;
			});
	},
	createRoom: async (args, req) => {
		if (!req.isAuth) {
			throw new Error('Unauthorized!');
		}

		const room = new Room({
			name: args.name,
			description: args.description,
			devices: [],
		});

		await room.save();

		await House.updateOne(
			{ userId: req.userId },
			{
				$push: { rooms: new mongoose.mongo.ObjectId(room._id) },
			}
		).catch(err => {
			throw err;
		});

		return await House.findOne({ userId: req.userId })
			.then(result => {
				return result;
			})
			.catch(err => {
				throw err;
			});
	},
	devices: async (parent, args, req) => {
		if (!req.isAuth) {
			throw new Error('Unauthorized!');
		}

		return await Device.find({ _id: parent.devices })
			.then(result => {
				return result;
			})
			.catch(err => {
				throw err;
			});
	},
	updateDevice: async (args, req) => {
		if (!req.isAuth) {
			throw new Error('Unauthorized!');
		}

		await Device.findByIdAndUpdate(args.id, args, { new: true })
			.then(result => {
				return result;
			})
			.catch(err => {
				throw err;
			});

		return await House.findOne({ userId: req.userId })
			.then(result => {
				return result;
			})
			.catch(err => {
				throw err;
			});
	},
};
