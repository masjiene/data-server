const houseSchema = require('./house.schema');
const authSchema = require('./auth.schema');
const { mergeSchemas } = require('graphql-tools');

module.exports = mergeSchemas({
	schemas: [houseSchema, authSchema],
	// onTypeConflict: (left, right) => right
});
