const graphql = require('graphql');
const authResolver = require('../resolvers/auth.resolver');

const {
	GraphQLObjectType,
	GraphQLString,
	GraphQLSchema,
	GraphQLID,
	GraphQLList,
	GraphQLNonNull,
} = graphql;

const UserType = new GraphQLObjectType({
	name: 'User',
	fields: () => ({
		_id: { type: GraphQLID },
		userId: { type: GraphQLID },
		email: { type: GraphQLString },
		password: { type: GraphQLString },
	}),
});

const AuthType = new GraphQLObjectType({
	name: 'Auth',
	fields: () => ({
		userId: { type: GraphQLID },
		token: { type: GraphQLString },
		tokenExpiration: { type: GraphQLString },
	}),
});

const RootQuery = new GraphQLObjectType({
	name: 'RootQueryType',
	fields: {
		login: {
			type: AuthType,
			args: {
				email: { type: new GraphQLNonNull(GraphQLString) },
				password: { type: new GraphQLNonNull(GraphQLString) },
			},
			resolve(parent, args, req) {
				return authResolver.login(args, req);
			},
		},
		users: {
			type: new GraphQLList(UserType),
			resolve(parent, args, req) {
				return authResolver.users(args, req);
			},
		},
		logout: {
			type: AuthType,
			args: {},
			resolve(parent, args, req) {
				return authResolver.logout(args, req);
			},
		},
	},
});

const Mutations = new GraphQLObjectType({
	name: 'Mutation',
	fields: {
		signup: {
			type: UserType,
			args: {
				email: { type: new GraphQLNonNull(GraphQLString) },
				password: { type: new GraphQLNonNull(GraphQLString) },
			},
			resolve(parent, args, req) {
				return authResolver.createUser(args, req);
			},
		},
		deleteUser: {
			type: UserType,
			args: {
				email: { type: new GraphQLNonNull(GraphQLString) },
			},
			resolve(parent, args, req) {
				return authResolver.deleteUser(args, req);
			},
		},
	},
});

module.exports = new GraphQLSchema({
	query: RootQuery,
	mutation: Mutations,
});
