const graphql = require('graphql');
const houseResolver = require('../resolvers/house.resolver');

const {
	GraphQLObjectType,
	GraphQLInputObjectType,
	GraphQLString,
	GraphQLSchema,
	GraphQLID,
	GraphQLInt,
	GraphQLList,
	GraphQLNonNull,
} = graphql;

const LocationType = new GraphQLObjectType({
	name: 'Location',
	fields: () => ({
		lat: { type: GraphQLString },
		long: { type: GraphQLString },
	}),
});

const DetailsType = new GraphQLObjectType({
	name: 'Details',
	fields: () => ({
		name: { type: GraphQLString },
		description: { type: GraphQLString },
		location: { type: LocationType },
	}),
});

const DeviceType = new GraphQLObjectType({
	name: 'Device',
	fields: () => ({
		_id: { type: GraphQLID },
		name: { type: GraphQLString },
		address: { type: GraphQLString },
		state: { type: GraphQLInt },
		command: { type: GraphQLString },
		type: { type: GraphQLString },
		port: { type: GraphQLString },
	}),
});

const RoomType = new GraphQLObjectType({
	name: 'Room',
	fields: () => ({
		_id: { type: GraphQLID },
		name: { type: GraphQLString },
		description: { type: GraphQLString },
		devices: {
			type: new GraphQLList(DeviceType),
			async resolve(parent, args, req) {
				return await houseResolver.devices(parent, args, req);
			},
		},
	}),
});

const HouseType = new GraphQLObjectType({
	name: 'House',
	fields: () => ({
		_id: { type: GraphQLID },
		userId: { type: GraphQLID },
		details: {
			type: DetailsType,
		},
		rooms: {
			type: new GraphQLList(RoomType),
			async resolve(parent, args, req) {
				return await houseResolver.rooms(parent, args, req);
			},
		},
	}),
});

const Query = new GraphQLObjectType({
	name: 'RootQueryType',
	fields: {
		house: {
			type: HouseType,
			async resolve(parent, args, req) {
				return await houseResolver.house(args, req);
			},
		},
	},
});

const LocationInputType = new GraphQLInputObjectType({
	name: 'LocationInput',
	fields: () => ({
		lat: { type: new GraphQLNonNull(GraphQLString) },
		long: { type: new GraphQLNonNull(GraphQLString) },
	}),
});

const DetailInputType = new GraphQLInputObjectType({
	name: 'DetailInput',
	fields: () => ({
		name: { type: new GraphQLNonNull(GraphQLString) },
		description: { type: new GraphQLNonNull(GraphQLString) },
		location: { type: LocationInputType },
	}),
});

const Mutation = new GraphQLObjectType({
	name: 'Mutation',
	fields: {
		createHouse: {
			type: HouseType,
			args: {
				details: { type: DetailInputType },
			},
			async resolve(parent, args, req) {
				return await houseResolver.createHouse(args, req);
			},
		},
		createRoom: {
			type: HouseType,
			args: {
				name: { type: new GraphQLNonNull(GraphQLString) },
				description: { type: GraphQLString },
			},
			async resolve(parent, args, req) {
				return await houseResolver.createRoom(args, req);
			},
		},
		updateDevice: {
			type: HouseType,
			args: {
				id: { type: new GraphQLNonNull(GraphQLID) },
				name: { type: GraphQLString },
				address: { type: GraphQLString },
				state: { type: GraphQLInt },
				command: { type: GraphQLString },
				type: { type: GraphQLString },
				port: { type: GraphQLString },
			},
			async resolve(parent, args, req) {
				return await houseResolver.updateDevice(args, req);
			},
		},
	},
});

module.exports = new GraphQLSchema({
	query: Query,
	mutation: Mutation,
});
