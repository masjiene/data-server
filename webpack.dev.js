const webpackDashboard = require('webpack-dashboard/plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
	.BundleAnalyzerPlugin;
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const merge = require('webpack-merge');

const common = require('./webpack.common.js');

module.exports = merge(common, {
	mode: 'development',
	plugins: [
		new webpackDashboard({ port: 4001 }),
		new BundleAnalyzerPlugin({
			analyzerMode: 'server',
			generateStatsFile: true,
			statsOptions: { source: true },
			analyzerPort: 4002,
		}),
		new FriendlyErrorsWebpackPlugin(),
	],
});
