const chai = require('chai');

const expect = chai.expect;
const request = require('supertest');
let agent;

describe('The swyser.data server', () => {
	before(done => {
		process.env.MONGO_USER = 'datanator';
		process.env.MONGO_PASSWORD = '!!skibidi34';
		process.env.MONGO_DB = 'swyserdb';

		const app = require('../src/index');
		agent = request.agent(app);

		done();
	});

	describe('Given the GraphQl endpoint', () => {
		describe('When sending a POST to the /api/login route is called', () => {
			it('it should log the user in if credentials are correct', done => {
				agent
					.post('/api')
					.send({
						query:
							'query { login(email: "swyser@live.co.za", password:"swyser") { userId token tokenExpiration } }',
					})
					.set('Accept', 'application/json')
					.expect(200)
					.end((err, res) => {
						if (err) return done(err);
						expect(JSON.parse(res.text).data.login).to.have.keys([
							'token',
							'tokenExpiration',
							'userId',
						]);
						expect(JSON.parse(res.text)).to.not.be.null;
						expect(JSON.parse(res.text)).to.not.equal(null);
						done();
					});
			});
		});
	});
}).timeout(15000);
